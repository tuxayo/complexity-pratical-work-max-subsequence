CC= g++ 

CFLAGS= -Wall -pedantic -std=c++11 -O2
#-Wextra

#TARGET= ./bin/tp1

OBJECTS= tp1.o result.o

tp1: $(OBJECTS)
	$(CC) $(CFLAGS) -o tp1 $(OBJECTS)

tp1.o: tp1.cpp
	$(CC) $(CFLAGS) -c tp1.cpp

result.o: result.cpp result.h
	$(CC) $(CFLAGS) -c result.cpp

clean:
	rm *.o
	rm tp1
