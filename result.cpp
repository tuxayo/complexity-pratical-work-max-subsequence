#include "result.h"

Result::Result(int value, int ptBegin, int ptEnd):value(value),ptBegin(ptBegin),ptEnd(ptEnd)
{
}

Result::Result()
{
    value = 0;
    ptBegin = 0;
    ptEnd = 0;
}

Result::Result(const Result& other):
value(other.value),
ptBegin(other.ptBegin),
ptEnd(other.ptEnd)
{
}

