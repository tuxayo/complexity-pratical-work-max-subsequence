#include <iostream>
#include <vector>
#include <cassert>

#include "result.h"

/* ALGO 1
n = array.size

current_max_sum = array[0]
current_max_begin = 0
current_max_end = 0
current_end_index_of_tested_inter = 0

for current_begin_index_of_tested_inter from 0 to (n - 1)

    for current_end_index_of_tested_inter from current_begin_index_of_tested_inter to (n - 1)
        sum = sum_range(array,
                        current_begin_index_of_tested_inter,
                        current_end_index_of_tested_inter)
        if sum > max
            current_max_sum = sum
            current_max_begin = current_begin_index_of_tested_inter
            current_max_end = current_end_index_of_tested_inter


return (current_max_begin, current_max_end)

*/

/* ALGO 2
n = array.size

current_max_sum = array[0]
current_max_begin = 0
current_max_end = 0

for current_begin_index_of_tested_inter from 0 to (n - 1)

    sum = array[current_begin_index_of_tested_inter]

    if sum > max
        current_max_sum = sum
        current_max_begin = current_begin_index_of_tested_inter
        current_max_end = current_end_index_of_tested_inter REPLACED BY CURRENT_BEGIN_INDEX

    for current_end_index_of_tested_inter PLUS ONE from current_begin_index_of_tested_inter to (n - 1)

        sum += array[current_end_index_of_tested_inter]

        if sum > max
            current_max_sum = sum
            current_max_begin = current_begin_index_of_tested_inter
            current_max_end = current_end_index_of_tested_inter


return (current_max_begin, current_max_end)

*/

int sum_range(std::vector<int>* vector, int ptBegin, int ptEnd){

    int accu = vector->at(ptBegin);

    if (ptBegin == ptEnd){
        return accu;
    }

    for(int i = ptBegin+1 ; i <= ptEnd; i++){
        accu += vector->at(i);
    }
    return accu;
}

Result sum_result_to_index(Result res, int index, std::vector<int>* vector){

    if (index > res.ptEnd){
        return Result(sum_range(vector,res.ptBegin,index),res.ptBegin,index);
    }
    else if (index < res.ptBegin){
        return Result(sum_range(vector,index,res.ptEnd),index,res.ptEnd);
    }
    else{
        return res;
    }
}

Result sum_result_to_result(Result res1, Result res2, std::vector<int>* vector){

    int minBegin = std::min(res1.ptBegin,res2.ptBegin);
    int maxEnd = std::max(res1.ptEnd,res2.ptEnd);
    
    return Result(sum_range(vector,minBegin,maxEnd),minBegin,maxEnd);
}

Result algoV1(std::vector<int>* vector) {

    int n = vector->size();

    int current_max_sum = vector->at(0);

    int current_max_begin = 0;
    int current_max_end = 0;

    for (int current_begin_index_of_tested_inter = 0 ; current_begin_index_of_tested_inter < n ; current_begin_index_of_tested_inter++){

        for (int current_end_index_of_tested_inter = current_begin_index_of_tested_inter ;  current_end_index_of_tested_inter < n ; current_end_index_of_tested_inter++){

            int sum = sum_range(vector,
                            current_begin_index_of_tested_inter,
                            current_end_index_of_tested_inter);

            if (sum > current_max_sum){
                current_max_sum = sum;
                current_max_begin = current_begin_index_of_tested_inter;
                current_max_end = current_end_index_of_tested_inter;
            }
        }
    }

    Result res(current_max_sum,current_max_begin,current_max_end);

    return res;
}

Result algoV2(std::vector<int>* vector) {

    int n = vector->size();

    int current_max_sum = vector->at(0);

    int current_max_begin = 0;
    int current_max_end = 0;

    int sum;

    for (int current_begin_index_of_tested_inter = 0 ; current_begin_index_of_tested_inter < n ; current_begin_index_of_tested_inter++){

        sum = vector->at(current_begin_index_of_tested_inter);

        if (sum > current_max_sum){
            current_max_sum = sum;
            current_max_begin = current_begin_index_of_tested_inter;
            current_max_end = current_begin_index_of_tested_inter;
        }

        for (int current_end_index_of_tested_inter = current_begin_index_of_tested_inter + 1 ;  current_end_index_of_tested_inter < n ; current_end_index_of_tested_inter++){

            sum += vector->at(current_end_index_of_tested_inter);

            if (sum > current_max_sum){
                current_max_sum = sum;
                current_max_begin = current_begin_index_of_tested_inter;
                current_max_end = current_end_index_of_tested_inter;
            }
        }
    }

    Result res(current_max_sum,current_max_begin,current_max_end);

    return res;
}


//v1 fausse
//<value,ptBeing,ptEnd> algo3(array<int> array) {

//
//     si array.size() > 1 alors
//         res1 = algo3(array.getPartie1());  //si impair sans le milieu
//         res2 = algo3(array.getPartie2();   //si impair sans le milieu
//
//         //todo tester milieu tout seul
//
//         temp1 = res1
//         temp2 = res2
//
//         si impair
//             si res1 joint array.middle()
//                 si res1+array.middle() > max alors temp1 = res1+array.middle()
//             si res2 joint array.middle()
//                 si res2+array.middle() > max alors temp2 = res2+array.middle()
//
//         si res1 joint res2 alors
//             max = temp1
//             si temp2 > max alors max = temp2
//             si temp1+temp2 > max alors max = temp1+temp2
//             si array.size() est impair

//         sinon
//             max = max(temp1,temp2)
//         retourner max
//     else
//         retourner <element_seul,indice,indice>
//
//
//
//}

//v2 building
//Result algoV3(std::vector<int>* vector){
//
//     if (vector.size() > 1){
//         Result res1 = algoV3(vector.getPartie1());
//         Result res2 = algoV3(vector.getPartie2());
//
//         Result temp1 = res1;
//         Result temp2 = res2;
//
//         Result max = std::max(temp1,temp2);
//
//         if (vector.size() % 2 == 1){
//
//             if (max < sum_result_to_index(res1,vector->size()/2,vector)){
//                 temp1 = sum_result_to_index(res1,vector->size()/2,vector);
//             }
//
//             if (max < sum_result_to_index(res2,vector->size()/2,vector)){
//                 temp1 = sum_result_to_index(res2,vector->size()/2,vector);
//             }
//                                        
//             max = std::max(max(temp1,temp2),sum_result_to_result(temp1, temp2, vector));
//             max = std::max(max, array.middle());
//         }
//     }
//         else{
//             max = std::max(max(temp1,temp2),sum_result_to_result(temp1, temp2, vector));
//         }
//
//         retourner max
//     else
//         retourner <element_seul,indice,indice>
//
//}


// BEGIN ALGO 3 V3
//<value, ptBegin, ptEnd> algo_3_v3(array) {
//    if array.size() == 1
//        return <element_seul, indice, indice>
//    res1 = algo_3_v3(get_first_half(array))
//    res2 = algo_3_v3(get_last_half(array))
//    res3 = max_sub_sequence_with_middle_element(array)
//    return max(res1, res2, res3)
//}
//
//// Note: could be optimized with the same logic as Aglo_2
//<value, ptBegin, ptEnd> max_sub_sequence_with_middle_element(array) {
//    int current_max_sum = vector->at(ptBegin);
//    int current_max_begin = 0;
//    int current_max_end = 0;
//
//    for (int i = ptBegin; i < ptMiddle; ++i) {
//        for(int j = ptMiddle; j < ptEnd+1; ++j) {
//            sum = sum_range(i,j);
//            if sum > current_max_sum
//                current_max_sum = sum
//                current_max_begin = i
//                current_max_end = j
//        }
//    }
//    return Result(current_max_sum, current_max_begin, current_max_end)
//}
//
// END ALGO 3 V3



// Note: could be optimized with the same logic as Algo_2
Result max_sub_sequence_with_middle_element(std::vector<int>* array) {
    int current_max_sum = array->at(0);
    int current_max_begin = 0;
    int current_max_end = 0;
    int sum;

    int middle = array->size()/2;
    for (int i = 0; i <= middle; ++i) {
        for(unsigned int j = middle; j < array->size(); ++j) {
            sum = sum_range(array, i ,j);
            if (sum > current_max_sum) {
                current_max_sum = sum;
                current_max_begin = i;
                current_max_end = j;
            }
        }
    }
    return Result(current_max_sum, current_max_begin, current_max_end);
}



Result algo_3_v3(std::vector<int>* array, int begin = 0, int end = -1) {
    if (end == -1) { end = array->size(); } // maybe off by one error

    if (array->size() == 1)
        return Result(array->at(0), begin, end);

    int middle = array->size()/2;
    Result res1 = algo_3_v3(array, 0, middle);
    //Result res2 = algo_3_v3(array, middle+1, array->size());
    //Result res3 = max_sub_sequence_with_middle_element(array);

//    return std::max(res1, std::max(res2, res3));
    return res1;
}
/* BEGIN ALGO 4 V1

algo_4_v1(array) {
    int current_max_sum = array->at(ptBegin);
    int current_max_begin = 0;
    int current_max_end = 0;

    for (int end_current_range = 0; end_current_range < array.size; ++end_current_range) {
        res = max_sub_sequence_with_last_element(array, end_current_range)
        if res.value > current_max_sum
            current_max_sum = res.value
            current_max_begin = res.ptBegin
            current_max_end = res.ptEnd
    }

    return Result(current_max_sum, current_max_begin, current_max_end)
}

max_sub_sequence_with_last_element(array, ptEnd) {

    int current_max_sum = array->at(ptBegin);
    int current_max_begin = 0;
    int current_max_end = 0;

    for (int i = 0; i <= ptEnd; ++i) {
        sum = sum_range(i,ptEnd);
        if sum > current_max_sum
            current_max_sum = sum
            current_max_begin = i
            current_max_end = j
    }
    return Result(current_max_sum, current_max_begin, current_max_end)
}

*/// END ALGO 4 V1
Result max_sub_sequence_with_last_element(std::vector<int>*array, int ptEnd) {

    int current_max_sum = array->at(0);
    int current_max_begin = 0;
    int current_max_end = 0;

    for (int i = 0; i <= ptEnd; ++i) {
        int sum = sum_range(array, i, ptEnd);
        if (sum > current_max_sum)
            current_max_sum = sum;
            current_max_begin = i;
            current_max_end = ptEnd; // TODO remove safely this useless line
    }
    return Result(current_max_sum, current_max_begin, current_max_end);
}

Result algo_4_v1(std::vector<int>* array) {
    int current_max_sum = array->at(0);
    int current_max_begin = 0;
    int current_max_end = 0;
    Result res;

    for (unsigned int end_current_range = 0; end_current_range < array->size(); ++end_current_range) {
        res = max_sub_sequence_with_last_element(array, end_current_range);
        if (res.value > current_max_sum)
            current_max_sum = res.value;
            current_max_begin = res.ptBegin;
            current_max_end = res.ptEnd;
    }

    return Result(current_max_sum, current_max_begin, current_max_end);
}


int main() {

    std::vector<int> test1 = { 8, -1, 2, 9 };
    std::vector<int> test2 = { 8, -9, 5, -1, 9 };
 
    //validation operator ==
//    Result result1(10,0,1);
//    Result result2(10,0,1);
//    
//    assert(result1 == result2);
    
    //validation operator <
//    Result result1(9,0,1);
//    Result result2(10,0,1);
//    
//    assert(result1 < result2);
    
    //validation operator <=
//    Result result1(11,0,1);
//    Result result2(10,0,1);
//    
//    assert(result1 <= result2);

    std::cout << " ---------------------" << std::endl;

    // ALGO 1
    std::cout << "testing algo1 :" << std::endl;
    Result res1 = algoV1(&test1);
    std::cout << "\ttest1 : " << res1 << std::endl;
    assert(res1.value == 18);

    Result res2 = algoV1(&test2);
    std::cout << "\ttest2 : " << res2 << std::endl;
    assert(res2.value == 13);
    std::cout << " ---------------------" << std::endl;

    // ALGO 2
    std::cout << "testing algo2 :" << std::endl;
    res1 = algoV1(&test1);
    std::cout << "\ttest1 : " << algoV2(&test1) << std::endl;
    assert(res1.value == 18);

    res2 = algoV1(&test2);
    std::cout << "\ttest2 : " << algoV2(&test2) << std::endl;
    assert(res2.value == 13);
<<<<<<< HEAD
    cout << " ---------------------" << endl;
/*
    // ALGO 3
    cout << "testing algo3 :" << endl;
    res1 = algo_3_v3(&test1);
    cout << "\ttest1 : " << res1 << endl;
    assert(res1.value == 18);
=======
    std::cout << " ---------------------" << std::endl;

    // ALGO 3
    std::cout << "testing algo3 :" << std::endl;
    //cout << "\ttest1 : " << algoV3(&test1) << std::endl;
    //cout << "\ttest2 : " << algoV3(&test2) << std::endl;
    std::cout << " ---------------------" << std::endl;
>>>>>>> 719f33a983388c02fa84a18ac4e72e98e6e47d4c

    res2 = algo_3_v3(&test2);
    cout << "\ttest2 : " << res2 << endl;
    assert(res2.value == 13);
    cout << " ---------------------" << endl;
*/
    // ALGO 4
    std::cout << "testing algo4 :" << std::endl;
    res1 = algo_4_v1(&test1);
    std::cout << "\ttest1 : " << res1 << std::endl;
    assert(res1.value == 18);

    res2 = algo_4_v1(&test2);
    std::cout << "\ttest2 : " << res2 << std::endl;
    assert(res2.value == 13);
    std::cout << " ---------------------" << std::endl;
    std::cout << "ALL TESTS PASSED" << std::endl;
    return 0;
}
