#ifndef _RESULT_H_
#define _RESULT_H_

#include <iostream>

struct Result
{
    int value;
    int ptBegin;
    int ptEnd;

    Result(int value, int ptBegin, int ptEnd);
    Result();
    
    Result(const Result& other);
    
    inline bool operator < (const Result& other)
    {
        return value < other.value;
    }
    
    inline bool operator == (const Result& other) 
    {
        return value == other.value;
    }
    
    inline bool operator <= (const Result& other)
    {
        return value <= other.value;
    }
};

inline std::ostream & operator<<(std::ostream & str, Result const & v) {

  str << "Max: " << v.value << ", ptBegin: " << v.ptBegin << ", ptEnd: " << v.ptEnd << std::flush;
  return str;
}

#endif
